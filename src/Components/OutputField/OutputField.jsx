import React, { useEffect, useState } from 'react'
import "./OutputField.css"

const OutputField = ({ content = false }) => {
    const [images, setImages] = useState({})
    function importAll(r) {
        let images = {};
        r.keys().forEach((item, index) => { images[item.replace('./', '')] = r(item); });
        return images
    }
    useEffect(() => {
        const _images = importAll(require.context("../../assets/individial_signs/", false, /\.(png|jpe?g|svg)$/))
        setImages(_images)
    }, [])

    return (
        <div className="output-field">
            <div className="text-area">
                {content !== undefined &&
                    content.length > 0 &&
                    content.map((image, i) => {
                        return (images[image] === undefined) ? false : <img key={`sign-img-${i}`} className='sign-img' alt={`The letter ${image[0]}`} src={images[image]} />
                    })
                }
            </div>
            <div className="output-border-bottom"></div>
        </div>
    )
}

export default OutputField