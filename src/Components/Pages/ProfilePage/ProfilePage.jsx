import React, { useContext } from 'react'
import { UserContext } from '../../../App'
import RecentTranslations from './RecentTranslations/RecentTranslations'
import "./ProfilePage.css"

const ProfilePage = () => {
	const { user } = useContext(UserContext)


	return (
		<div className="profile-page-container">

			{user !== undefined &&
				<div className="page-intro">
					<h2 className="profile-page-header">Welcome to your profile {user.username}</h2>
				</div>}
			<div className="center">
				<RecentTranslations />
			</div>
		</div>
	)
}

export default ProfilePage
